/**
* Control code inspired from Grolleau et al.
* Additional thread: hardware interrupt for speed measurement
* Core thread: all other functions
**/

#include "EasyCAT.h"                // EasyCAT library to interface the LAN9252
#include <SPI.h>                    // SPI library

EasyCAT EASYCAT(9);                 // EasyCAT instantiation

                                    // The constructor allow us to choose the pin used for the EasyCAT SPI chip select                    
                                    // We can choose between:
                                    // 8, 9, 10, A5, 6, 7 (Move the breaker accordingly on the EasyCAT shield                                   

/*
 *  Variables
 */
 
/* Set temperature */
const float setTemperature = 26.0;

/* Serial port baudrate*/
#define BDR 9600
 
/* Pin name affectation */
const int TemperaturePin = A0;
const int MotorPin = 3; /* PWM output connected with Timer 2*/
const int TachoPin = 2; /* Ouput for generating Hardware interrupt for speed measuring*/

/* Temperature in degrees*/
float temperature = 0.0; ;

/* Program duration storage: 0 for controling; 1 for sending */
unsigned long durees[2];

/* Control gain*/ 
const int gain = 20;

/* Time storage */
unsigned long currentAppliTime = 0 ;

/* Sensor value*/
int value = 0 ;

/* Motor control */
int control = 0 ;

/* Speed in rpm (roations per minute)*/
unsigned long rpm = 0;

/* Half tour duration */
volatile unsigned long dureedemitour = 1;

/* Buffers for sending data*/
char monbuffer[20];
char monbuffer1[80];

/* Variables holding statechart states */
#define Attente4 0
#define Attente3 1
#define Attente2 2
#define Attente1 3

/*
* Convert value read on temperature sensor into degrees Celcius
* value: value between [0-255] for voltage between [0-5V]
*/
float voltage2temperature(int value) {
  return (4.9*value-500)/10;
}

/* 
 *  Initialization thread
 */
void setup()
{
  if (EASYCAT.Init() == true)                                     // Initialization
  {                                                               // Succesfully completed
    Serial.print ("initialized");                              
  }                                                            
  
  else                                                            // Initialization failed   
  {                                                               // the EasyCAT board was not recognized
    Serial.print ("initialization failed");                        
                                                                   
  }  
  /* Timer 2 for Motor control PWM signal generation, duty cycle is defined by control */
  pinMode(MotorPin,OUTPUT);
  /* Fast PWM Mode, non inverted */
  TCCR2A = 0x27; 
  TCCR2B = 0x0A;
  /* Outuput A: prescale factor  for 16 Mhz/(OCR2A*8) frequency */
  OCR2A = 80;
  /* Duty cycle initialization */
  OCR2B = 1;
  
  /*Hardware interrupt for measuring speed from Tacho*/ 
  pinMode(TachoPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(TachoPin),tachoChange,CHANGE);
  
  /* Initialization of USB speed */ 
  Serial.begin(BDR); 
  
  /*Initial temperature*/
  temperature = voltage2temperature(analogRead(TemperaturePin));
}

/*
 * Additional thread:
 * ISR (Interrupt Service Routine) for Hardware interrupt for measuring fan speed
 * Statechart
 */
void tachoChange()
{
  static unsigned long t1 = 0;
  static unsigned long t2 = 0;
  static unsigned long t3 = 0;
  static unsigned long t4 = 0;
  static char etat = Attente4;
  unsigned long tmp = 0 ;
  
  bool state = 0;
  state = digitalRead(TachoPin);
  
  noInterrupts();   /* following code is non interruptible */
  tmp = micros();
  switch (etat) { //4->1->2->3->4
    case Attente4:
    etat = Attente1; //T1
    t4 = tmp;
    dureedemitour = t4-t2;
    break;
    case Attente3:
    etat = Attente4;
    t3 = tmp;
    dureedemitour = t3-t1;
    break;
    case Attente2:
    etat = Attente3; //T2
    t2 = tmp;
    dureedemitour = t2-t4;
    break;
    case Attente1:
    etat = Attente2;
    t1 = tmp;
    dureedemitour = t1-t3;
    break;
  }
  interrupts();   /* End of critical code*/
}


/*
 * Calculate speed from ISR, in rpm (rotations per minute): (60 secondes/2)/dureedemitour
 * dureedemitour is in µs
 */
unsigned long readSpeed()
{
  unsigned long d;
  noInterrupts();
  d = dureedemitour;
  interrupts();
  return 30000000/d;
}

/* 
 *  Send data on serial port
 */
void sendSignals(const float setTemp, float temp, unsigned long spd, int contrl)
{
  int dTemp,uTemp, dSetTemp, uSetTemp;
  
  /* Sepration unités et decimales des températures */

  //Température mesuréé
  uTemp = int(temp);
  dTemp = int((temp-uTemp)*100);

  //Température de consigne utilisateur  
  uSetTemp = int(setTemp);
  //19    =  int(19.752 )
  
  dSetTemp = int((setTemp-uSetTemp)*100);
  //  75   =       19.752-19=0.75  *100

  //Récupération de la vitesse de rotation du ventilateur
  spd = readSpeed();
    
  //Envoie des donnees sur l'interface série (sert au débuggage)  
  sprintf(monbuffer,"%d.%d,%d.%d,%u,%d\r\n",uSetTemp,dSetTemp,uTemp,dTemp,spd,contrl);
  Serial.write(monbuffer);
  Serial.flush();

  //Envoie des données par EtherCAT :
  EASYCAT.BufferIn.Byte[0] = uSetTemp; 
  EASYCAT.BufferIn.Byte[1] = dSetTemp; 
  EASYCAT.BufferIn.Byte[2] = uTemp;
  EASYCAT.BufferIn.Byte[3] = dTemp;
  EASYCAT.BufferIn.Byte[4] = (spd&0xFF00) >> 8;
  EASYCAT.BufferIn.Byte[5] = spd&0x00FF;
  EASYCAT.BufferIn.Byte[6] = contrl;

  /*
   * Comment sont organisées les données :
   * 
   *  EASYCAT.BufferIn.Byte[X] avec X représentant le numéro de l'octet de la donnée (entre 0 et 7)
   * 
   *  0 = Unités de la température réglée par l'utilisateur
   *  1 = Décimales (2) de la température réglée par l'utilisateur
   *  2 = Unités de la température mesurée
   *  3 = Décimales (2) de la température mesurée
   *  4 = Octet de poids fort pour la vitesse du ventilateur
   *  5 = Octet de poids faible pour la vitesse du ventilateur
   *  6 = Valeur de la commande (0 à 70)
   * 
   */
}

/*
 * Define duty cycle of PWM control signal generated by Timer 2
 * Duty cycle in [1-79]
 */
void controlMotor(unsigned int val)
{
  OCR2B = val;
}

/*
 * Function to saturate value
 * v: input value
 * Min: minimum output value
 * Max: maximum output value
 */
int saturateControl(int v, int Min, int Max)
{
  if (v>Max) return Max;
  if (v<Min) return Min;
  return v;
}


/*
 * Core thread: infinite loop
 */
void loop()                                             // In the main loop we must call ciclically the 
{                                                       // EasyCAT task and our application
                                                        //
                                                        // This allows the bidirectional exachange of the data
                                                        // between the EtherCAT master and our application
                                                        //
                                                        // The EasyCAT cycle and the Master cycle are asynchronous
                                                        //   

  EASYCAT.MainTask();                                   // execute the EasyCAT task
  
  Application();                                        // user applications
}

/*
 * User Application
 */
void Application()
{
  /* number of microseconds since Arduino board began running current program */
  currentAppliTime = micros();
  
  /* Acquire, filter and convert into degrees */
  value = analogRead(TemperaturePin);
  temperature = 0.8*temperature+0.2*voltage2temperature(value);
  
  /* Calculate, saturate and apply control */
  control = saturateControl((temperature-setTemperature)*gain,1, 79);
  controlMotor(control);
  
  /* Calculate control stage duration*/
  durees[0] = micros()-currentAppliTime;
  
  /* Read speed */
  rpm = readSpeed();
  
  /* Send signals to master*/
  sendSignals(setTemperature,temperature,rpm,control);
}
