/**
* Control code inspired from Grolleau et al.
* And inspired from AB&T examples
* Additional thread: hardware interrupt for speed measurement
* Core thread: all other functions
**/

#include "EasyCAT.h"                // EasyCAT library to interface the LAN9252
#include <SPI.h>                    // SPI library

EasyCAT EASYCAT;                    // EasyCAT istantiation

                                    // The constructor allow us to choose the pin used for the EasyCAT SPI chip select 
                                    // Without any parameter pin 9 will be used 
                   
                                    // We can choose between:
                                    // 8, 9, 10, A5, 6, 7                                    

                                    // On the EasyCAT board the SPI chip select is selected through a bank of jumpers              

                                    // (The EasyCAT board REV_A allows only pins 8, 9, 10 through 0 ohm resistors)

 //EasyCAT EASYCAT(8);              // example:                                  
                                    // pin 8 will be used as SPI chip select
                                    // The chip select chosen by the firmware must match the setting on the board  
 


/* Serial port baudrate*/
#define BDR 9600
char monbuffer[20];
 

/* 
 *  Initialization thread
 */
void setup()
{
  pinMode(A0, INPUT);
  if (EASYCAT.Init() == true)                                     // initialization
  {                                                               // succesfully completed
    Serial.print ("initialized");                                 //
  }                                                               //
  
  else                                                            // initialization failed   
  {                                                               // the EasyCAT board was not recognized
    Serial.print ("initialization failed");                       //     
                                                                  // The most common reason is that the SPI 
                                                                  // chip select choosen on the board doesn't 
  }                                                               // match the one choosen by the firmware

  /* Initialization of USB speed */ 
  Serial.begin(BDR); 

}



/*
 * User Application
 */
void Application()
{

  sprintf(monbuffer,"%d.%d,%d.%d,%u,%d\r\n",21,analogRead(A0)/4,25,472,18,255);
  Serial.write(monbuffer);
  
  Serial.flush();
  
  EASYCAT.BufferIn.Byte[0] = 21;
  EASYCAT.BufferIn.Byte[1] = analogRead(A0)/4;
  EASYCAT.BufferIn.Byte[2] = 25;
  EASYCAT.BufferIn.Byte[3] = 47;
  EASYCAT.BufferIn.Byte[4] = 18;
  EASYCAT.BufferIn.Byte[5] = 255;
  EASYCAT.BufferIn.Byte[6] = 16;
}

/*
 *  Main Loop
 */
void loop()                                             // In the main loop we must call ciclically the 
{                                                       // EasyCAT task and our application
                                                        //
                                                        // This allows the bidirectional exachange of the data
                                                        // between the EtherCAT master and our application
                                                        //
                                                        // The EasyCAT cycle and the Master cycle are asynchronous
                                                        //   

  EASYCAT.MainTask();                                   // execute the EasyCAT task
  
  Application();                                        // user applications
}
