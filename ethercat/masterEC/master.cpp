 /* *********************************************************************
* Receive signals over USB port and display them
*
*  *********************************************************************
*/


#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "ethercat.h"

#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>

#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions



//Interface Ethernet utilisée pour la connexion EtherCAT
char myEth[] = "eth0";

/* EtherCAT definitions */
#define EC_TIMEOUTMON 500

char IOmap[4096];
OSAL_THREAD_HANDLE thread1;
int expectedWKC;
boolean needlf;
volatile int wkc;
boolean inOP;
uint8 currentgroup = 0;

int i, j, oloop, iloop, chk;

/* Structure used to store the data from the EtherCAT bus */
struct EthData {
   unsigned char Input[8];  // 8 bytes for the inputs
   unsigned char Output[8]; // 8 bytes for the outputs
};

/* Timer spec*/
timer_t timer = (timer_t) 0;
long int periode = 100 ;
struct sigevent event;
struct itimerspec spec;

void handler_EC(int no) ; // Definition of the function that will handle EtherCAT

/* Function to initialization the EtherCAT bus*/
void EC_initialization(char *ifname)
{

   /* initialise SOEM, bind socket to ifname */
   if (ec_init(ifname))
   {
      printf("ec_init on %s succeeded.\n",ifname);
      
      /* find and auto-config slaves */
       if ( ec_config_init(FALSE) > 0 )
      {
         printf("%d slaves found and configured.\n",ec_slavecount);

         ec_config_map(&IOmap);

         ec_configdc();

         printf("Slaves mapped, state to SAFE_OP.\n");
         /* wait for all slaves to reach SAFE_OP state */
         ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4);

         oloop = ec_slave[0].Obytes;
         if ((oloop == 0) && (ec_slave[0].Obits > 0)) oloop = 1;
         if (oloop > 8) oloop = 8;
         iloop = ec_slave[0].Ibytes;
         if ((iloop == 0) && (ec_slave[0].Ibits > 0)) iloop = 1;
         if (iloop > 8) iloop = 8;

         printf("segments : %d : %d %d %d %d\n",ec_group[0].nsegments ,ec_group[0].IOsegment[0],ec_group[0].IOsegment[1],ec_group[0].IOsegment[2],ec_group[0].IOsegment[3]);

         printf("Request operational state for all slaves\n");
         expectedWKC = (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC;
         printf("Calculated workcounter %d\n", expectedWKC);
         ec_slave[0].state = EC_STATE_OPERATIONAL;
         /* send one valid process data to make outputs in slaves happy*/
         ec_send_processdata();
         ec_receive_processdata(EC_TIMEOUTRET);
         /* request OP state for all slaves */
         ec_writestate(0);
         chk = 200;
         /* wait for all slaves to reach OP state or chk to reach 0 */
         do
         {
            ec_send_processdata();
            ec_receive_processdata(EC_TIMEOUTRET);
            ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
         }
         while (chk-- && (ec_slave[0].state != EC_STATE_OPERATIONAL));
      }
      else
        {
            printf("No slaves found!\n");
        }
   }
   else
    {
        printf("No socket connection on %s\nExecute as root\n",ifname);
    }
}

EthData EC_read()
{
   EthData EthRead;
   if (ec_slave[0].state == EC_STATE_OPERATIONAL ) //Si l'esclave est en mode opérationnel
         {
            inOP = TRUE;
               ec_send_processdata();
               wkc = ec_receive_processdata(EC_TIMEOUTRET);

                    if(wkc >= expectedWKC) // Check if the communication was successful according to the datagram
                    {

                        for(j = 0 ; j < oloop; j++){EthRead.Output[j] = *(ec_slave[0].outputs + j);} // Read the Outputs
                        for(j = 0 ; j < iloop; j++){EthRead.Input[j] = *(ec_slave[0].inputs + j);}   // Read the Inputs
                        needlf = TRUE;
                    }
                    osal_usleep(5000);
                inOP = FALSE;
            }
            else
            {
                printf("Not all slaves reached operational state.\n");
                ec_readstate();
                for(i = 1; i<=ec_slavecount ; i++)
                {
                    if(ec_slave[i].state != EC_STATE_OPERATIONAL)
                    {
                        printf("Slave %d State=0x%2.2x StatusCode=0x%4.4x : %s\n",
                            i, ec_slave[i].state, ec_slave[i].ALstatuscode, ec_ALstatuscode2string(ec_slave[i].ALstatuscode));
                    }
                }
            }

   return EthRead;
}

//Ferme la connexion EtherCAT
void EC_close()
{
   
   printf("\nRequest init state for all slaves\n");
   ec_slave[0].state = EC_STATE_INIT;
   /* request INIT state for all slaves */
   ec_writestate(0);

   printf("End simple test, close socket\n");
   /* stop SOEM, close socket */
   ec_close();    
}

OSAL_THREAD_FUNC ecatcheck( void *ptr )
{
    int slave;
    (void)ptr;                  /* Not used */

    while(1)
    {
        if( inOP && ((wkc < expectedWKC) || ec_group[currentgroup].docheckstate))
        {
            if (needlf)
            {
               needlf = FALSE;
               printf("\n");
            }
            /* one ore more slaves are not responding */
            ec_group[currentgroup].docheckstate = FALSE;
            ec_readstate();
            for (slave = 1; slave <= ec_slavecount; slave++)
            {
               if ((ec_slave[slave].group == currentgroup) && (ec_slave[slave].state != EC_STATE_OPERATIONAL))
               {
                  ec_group[currentgroup].docheckstate = TRUE;
                  if (ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR))
                  {
                     printf("ERROR : slave %d is in SAFE_OP + ERROR, attempting ack.\n", slave);
                     ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
                     ec_writestate(slave);
                  }
                  else if(ec_slave[slave].state == EC_STATE_SAFE_OP)
                  {
                     printf("WARNING : slave %d is in SAFE_OP, change to OPERATIONAL.\n", slave);
                     ec_slave[slave].state = EC_STATE_OPERATIONAL;
                     ec_writestate(slave);
                  }
                  else if(ec_slave[slave].state > EC_STATE_NONE)
                  {
                     if (ec_reconfig_slave(slave, EC_TIMEOUTMON))
                     {
                        ec_slave[slave].islost = FALSE;
                        printf("MESSAGE : slave %d reconfigured\n",slave);
                     }
                  }
                  else if(!ec_slave[slave].islost)
                  {
                     /* re-check state */
                     ec_statecheck(slave, EC_STATE_OPERATIONAL, EC_TIMEOUTRET);
                     if (ec_slave[slave].state == EC_STATE_NONE)
                     {
                        ec_slave[slave].islost = TRUE;
                        printf("ERROR : slave %d lost\n",slave);
                     }
                  }
               }
               if (ec_slave[slave].islost)
               {
                  if(ec_slave[slave].state == EC_STATE_NONE)
                  {
                     if (ec_recover_slave(slave, EC_TIMEOUTMON))
                     {
                        ec_slave[slave].islost = FALSE;
                        printf("MESSAGE : slave %d recovered\n",slave);
                     }
                  }
                  else
                  {
                     ec_slave[slave].islost = FALSE;
                     printf("MESSAGE : slave %d found\n",slave);
                  }
               }
            }
            if(!ec_group[currentgroup].docheckstate)
               printf("OK : all slaves resumed OPERATIONAL.\n");
        }
        osal_usleep(10000);
    }
}


/* ********************************************************************
* Main thread
* 
* *********************************************************************
*/
int main (void)
{
   //printf("SOEM (Simple Open EtherCAT Master)\nSimple Read test\n");
   osal_thread_create(&thread1, 128000, (void**)&ecatcheck, (void*) &ctime); // Créée le thread pour la gestion de la communication EtherCAT
      
   EC_initialization(myEth); //On initialise la connexion EtherCAT sur le dispositif myEth ("eth0" dans notre cas)

	/* Configurer le timer */
	signal (SIGRTMIN, handler_EC);
	event.sigev_notify = SIGEV_SIGNAL;
	event.sigev_signo  = SIGRTMIN;
	periode = periode*1000000 ;	/* conversion en ns */
	spec.it_interval.tv_sec  = periode/1000000000;
	spec.it_interval.tv_nsec = periode % 1000000000;	
	spec.it_value = spec.it_interval;
	
   /* Allouer le timer */
	if (timer_create(CLOCK_REALTIME, & event, & timer) != 0) {
		perror("timer_create");
		exit(EXIT_FAILURE);
	} 
	/* Programmer le timer */
	if (timer_settime(timer, 0, &spec, NULL) != 0) {
		perror("timer_settime");
		exit(EXIT_FAILURE);
	}
	
	/*  Pause until occurrence of timer*/
	while (1){pause() ;}


	EC_close(); // Ferme la connexion EtherCAT
	return 0;
}

void handler_EC(int no){
   EthData EthArduino; 
   
   
	EthArduino = EC_read(); // On stocke les donnéees lues sur EtherCAT dans la structure EthArduino;
   
	printf("Set Temp: %d.%d | UTemp: %d.%d | Speed: %d | Control: %d\n", EthArduino.Input[0],EthArduino.Input[1],EthArduino.Input[2],EthArduino.Input[3],(EthArduino.Input[4]<<8)+EthArduino.Input[5],EthArduino.Input[6]);
   /*
    * Comment sont organisées les données :
    * 
    *   EthArduino.Input[X] avec X représentant le numéro de l'octet de la donnée (entre 0 et 7)
    * 
    *  0 = Unités de la température réglée par l'utilisateur
    *  1 = Décimales (2) de la température réglée par l'utilisateur
    *  2 = Unités de la température mesurée
    *  3 = Décimales (2) de la température mesurée
    *  4 = Octet de poids fort pour la vitesse du ventilateur
    *  5 = Octet de poids faible pour la vitesse du ventilateur
    *  6 = Valeur de la commande (0 à 70)
    * 
    */
}
