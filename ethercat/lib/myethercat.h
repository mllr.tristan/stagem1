#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h>

#include "ethercat.h"

//#ifndef _myethercat_
//#define _myethercat_

#ifdef __cplusplus
extern "C"
{
#endif


void simpletest(char *ifname);

void EC_read();

void EC_close();

OSAL_THREAD_FUNC ecatcheck( void *ptr );

#ifdef __cplusplus
}
#endif

//#endif