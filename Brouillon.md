
# EtherCAT

La première partie du stage consiste a changer le moyen de communication entre une Raspberry Pi et une Arduino dans le TP d'informatique industrielle de M1. 

Le TP consiste en un mini projet ayant pour but de créer un système de supervision 

J'ai procédé comme suit:
- Test de communication entre l'Arduino et PC avec l'ensemble du circuit
- Création d'un programme Arduino reproduisant le comportement du circuit pour tester facielement
- Création d'un Makefile pour compiler SOEM en dehors de son répertoire d'origine
- Création d'un programme remplacant l'interface série par Ethercat dans le programme master.

Passage de micros à millis pour résoudre les problèmes de vitesse de ventilateur constante.

Enfin, j'ai tenté différentes méthodes pour réduire la latence dans l'échange d'images entre le serveur et le client :
- Image en niveau de gris pour passer de 3 à 1 canal
