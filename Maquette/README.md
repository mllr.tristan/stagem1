Ce dossier contient l'ensemble des fichiers nécessaires à la fabrication des maquettes pour le TP EtherCAT

## La planche :

La planche et ses pieds (9 par placnhes) peuvent être découpés au laser dans une planche en bois (MDF disponible chez Leroy Merlin par exemple) de 3 ou 6 mm d'épaisseur et d'une dimension d'au moins **550 par 250 mm** ou bien dans du plexiglass. Il faudras donc choisir l'un des 2 dossiers en fonction de l'épaisseur du matériau. 

Les fichiers .dxf , .pdf et .dwg peuvent être utilisés pour la découpe laser, ceux au format .step et .stl sont les modèles 3D que vous pouvez modifier.

Le MDF 3mm est plutôt fragile pour une planche de cette taille, il faudras donc idéalement passer sur du 6mm. Ou bien, comme proposé par Patricia Hirtz, choisir une plaque en bakélite comme celle-ci : https://fr.rs-online.com/web/p/accessoires-de-boitiers/2171172/ et y percer manuellement les trous pour les vis.

Malheuresement, je pense que cette planche est trop petite, et je n'ai pas trouvé de plaque à la bonne taille chez RS-online.

## Les supports imprimés en 3D :

Dans le dossiers Impression_3D se trouve l'ensemble des fichiers nécessaires pour imprimer les supports en 3D :
- RPiRPi est le support  portant les 2 Raspberry Pis sur le switch (il en faut 2 par maquettes)
- Barres de maintien est le fichier contenant les barres fixant les 2 parties du support précédant (il en faut aussi 2 par maquettes)
- SupportPiCam est le fichier pour le support de la caméra (il en faut 1 par maquettes)
- SupportVentilo est une pièce à faire passer au travers de la planche pour fixer le ventilateur (il en faut 2 par maquettes)
